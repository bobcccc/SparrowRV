# 搭建开发环境

## 简介
想要使用小麻雀处理器，至少需要在Windows系统上安装以下软件

### Python3
无论是仿真还是上FPGA，Python3是必须安装的，不能使用Python2  
可进入[Python官网](https://www.python.org/)，下载并安装Python 3.x版本(建议使用稳定版)  
Linux系统：`sudo apt install python3 python3-tk`

### MounRiver Studio(MRS)
沁恒针对RV处理器搞的集成开发环境(IDE)，界面很友好，开箱即用，非常推荐  
可进入[MRS官网](http://www.mounriver.com/)，下载并安装  

### iverilog
可以看[这里](/doc/仿真手册/安装iverilog仿真环境.md)  

## 非必需软件
非必需软件与必须软件是替代关系，不是很推荐使用  

### Make
make可以在Win和Linux系统上工作，取代编译软件的`MRS`和控制仿真的`Batchfile批处理`，操作方式如下  
通过makefile脚本，仅需终端输入make，即可执行自动化编译。虽然写脚本有点麻烦，但是后期用得爽。    
使用流程：  
1. 下载并解压GCC工具链至`/tools/`目录，GCC请根据操作系统(Win/Linux)进行选择：  
百度网盘：https://pan.baidu.com/s/1thofSUOS5Mg0Fu-38qPeag?pwd=dj8b  
Github：https://github.com/xiaowuzxc/SparrowRV/releases/tag/v0.8   
请确保解压后文件目录为以下形式，否则无法正常make   
```
SparrowRV
  ├─bsp
  ├─doc
  ├─pic
  ├─rtl
  ├─tb
  └─tools
      └─RISC-V_Embedded_GCC
         ├─bin
         ├─distro-info
         ├─doc
         ├─include
         ├─lib
         ├─libexec
         ├─riscv-none-embed
         └─share
```
2. (Linux或已安装make的windows用户可跳过)下载上方GCC工具链中的make.exe，将make.exe所在的路径添加至环境变量`Path`，添加环境变量的步骤自行百度。  
3. 进入`/bsp/app/`，终端输入`make`，执行编译，此目录下会输出文件  
4. 进入`/bsp/app/`，终端输入`make clean`，清理编译文件  

### Modelsim
替代iverilog的仿真软件
